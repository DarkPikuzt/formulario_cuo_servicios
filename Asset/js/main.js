



$(document).ready(function() {
    $( "#enviar" ).click(function() {
        let url = "Controller/cCrud.php";
        let datos = new FormData(document.getElementById("form"));
        let data = $('#form').serialize();
        let fecha = $('#fecha').val();
        let nombre = $('#nombre').val();
        let email = $('#email').val();
        let solicitante = $('#solicitante').val();
        let dependencia = $('#dependencia').val();
        let region = $('#region').val();
        let area = $('#area').val();
        let informacion = $('#informacion').val();
        let descripcion = $('#descripcion').val();
        let proyecto = $('#proyecto').val();
        let contexto = $('#contexto').val();
        let op = $('input[name=op]:checked').val();
        let validar = $('#op').val();
        console.log(op)

        console.log(data)

        let bandera = false;
        if (op === 'Asesoria' || op === 'Informacion'){
            bandera= true;
            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();
        }else{

            $('#error.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#error').addClass('has-error');
            $('#error').append('<strong class="col-12 help-block " style="color: #CC0000">Es seleccionar el tipo.</strong>');
            //$('#fecha').css("border-color", "red");
            return false;
        }

        if (fecha.length <= 0) {
            $('#error.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#error').addClass('has-error');
            $('#error').append('<strong class="col-12 help-block " style="color: #CC0000">Es necesario la fecha.</strong>');
            $('#fecha').css("border-color", "red");
            return false;
        } else {
            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('#fecha').css("border-color", "");
        }

        if (nombre.length <= 0) {
            $('#error.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#error').addClass('has-error');
            $('#error').append('<strong class="col-12 help-block " style="color: #CC0000">Es necesario el nombre.</strong>');
            $('#nombre').css("border-color", "red");
            return false;
        } else {
            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('#nombre').css("border-color", "");
        }

        if (email.length <= 0) {
            $('#error.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#error').addClass('has-error');
            $('#error').append('<strong class="col-12 help-block " style="color: #CC0000">Es necesario el email.</strong>');
            $('#email').css("border-color", "red");
            return false;
        } else {
            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('#email').css("border-color", "");
        }

        if (solicitante === 'Seleccionar') {
            $('#error.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#error').addClass('has-error');
            $('#error').append('<strong class="col-12 help-block " style="color: #CC0000">Es necesarion seleccionar  solicitante.</strong>');
            $('#solicitante').css("border-color", "red");
            return false;
        } else {

            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('#solicitante').css("border-color", "");


        }

        if (dependencia.length <= 0) {
            $('#error.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#error').addClass('has-error');
            $('#error').append('<strong class="col-12 help-block " style="color: #CC0000">Es necesaria la identidad o dependencia.</strong>');
            $('#dependencia').css("border-color", "red");
            return false;
        } else {
            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('#dependencia').css("border-color", "");
        }

        if (region === 'Seleccionar') {
            $('#error.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#error').addClass('has-error');
            $('#error').append('<strong class="col-12 help-block " style="color: #CC0000">Es necesario seleccionar  una region.</strong>');
            $('#region').css("border-color", "red");
            return false;
        } else {
            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('#region').css("border-color", "");


        }

        if (area === 'Seleccionar') {
            $('#error.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#error').addClass('has-error');
            $('#error').append('<strong class="col-12 help-block " style="color: #CC0000">Es necesario seleccionar un area.</strong>');
            $('#area').css("border-color", "red");
            return false;
        } else {
            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('#area').css("border-color", "");


        }

        if (informacion === 'Seleccionar') {
            $('#error.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#error').addClass('has-error');
            $('#error').append('<strong class="col-12 help-block " style="color: #CC0000">Es necesaria seleccionar la informacion.</strong>');
            $('#informacion').css("border-color", "red");
            return false;
        } else {
            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('#informacion').css("border-color", "");
        }



        if (descripcion.length <= 0) {
            $('#error.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#error').addClass('has-error');
            $('#error').append('<strong class="col-12 help-block " style="color: #CC0000">Es necesaria la descripción.</strong>');
            $('#descripcion').css("border-color", "red");
            return false;
        } else {
            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('#descripcion').css("border-color", "");
        }

        if (proyecto.length <= 0) {
            $('#error.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#error').addClass('has-error');
            $('#error').append('<strong class="col-12 help-block " style="color: #CC0000">Es necesaria el nombre del proyecto.</strong>');
            $('#proyecto').css("border-color", "red");
            return false;
        } else {
            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('#proyecto').css("border-color", "");
        }

        if (contexto === 'Seleccionar') {
            $('#error.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#error').addClass('has-error');
            $('#error').append('<strong class="col-12 help-block " style="color: #CC0000">Es necesario seleccionar el contexto del proyecto.</strong>');
            $('#contexto').css("border-color", "red");
            return false;
        } else {
            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('#contexto').css("border-color", "");
        }
        $.ajax({
            data: data,
            type: "POST",
            dataType: "html",
            url: url,
            success:function (data) {
                console.log(data)
                $('#modal').modal('show')
               $("#form")[0].reset()
            }
        });

    });
});


/* login */

$(document).ready(function() {
    $( "#ingresar" ).click(function() {
        let url = "Controller/cLogin.php";
        let data = $('#formLogin').serialize();
        $.ajax({
            data: data,
            type: "POST",
            dataType: "html",
            url: url,
            success:function (data) {
                console.log(data)

                if (data === '1'){
                    window.location.href = "admin.php";
                }else{

                }
            }
        });

    });
});

$(document).ready(function() {
    $('#solicitante').change(function() {
        if (this.value == 'otro') {
            $("#otro_solicitante").toggle(true)
        } else {
            $("#otro_solicitante").toggle(false)

        }
    })
    $('#contexto').change(function() {
        if (this.value == 'Otro') {
            $("#otro_contexto").toggle(true)
        } else {
            $("#otro_contexto").toggle(false)

        }
    })


    $('#email').change(function () {

        let correo = $('#email').val();
        let arrayEmail = Array.from(correo);
        let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@uv.mx$/;
        let patt = new RegExp(re)
        if (patt.test(correo)){
            $('#error.has-error').removeClass('has-error');
            $('.help-block').remove();

        }else {
            $('#emailError.has-error').removeClass('has-error ');
            $('.help-block').remove();
            $('#emailError').addClass('has-error');
            $('#emailError').append('<strong class="col-12 help-block " style="color: #CC0000">Solo se almite correo uv "ejemplo@uv.mx".</strong>');

        }
    })
});


$(document).ready(function () {
    $('#example').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla.",
            "info": "Del _START_ al _END_ de _TOTAL_ ",
            "infoEmpty": "Mostrando 0 registros de un total de 0.",
            "infoFiltered": "(filtrados de un total de _MAX_ registros)",
            "infoPostFix": "(actualizados)",
            "lengthMenu": "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "searchPlaceholder": "Dato para buscar",
            "zeroRecords": "No se han encontrado coincidencias.",
            "paginate": {
                "first": "Primera",
                "last": "Última",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": "Ordenación ascendente",
                "sortDescending": "Ordenación descendente"
            }
        }
    });
});




