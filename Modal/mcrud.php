<?php
include_once '../Config/DB.php';
require_once 'PHPMailer/Exception.php';
require_once 'PHPMailer/PHPMailer.php';
require_once 'PHPMailer/SMTP.php';
require_once 'fpdf182/fpdf.php';


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


class mcrud
{
    public $nombre;
    public $fecha;
    public $solicitante;
    public $dependencia;
    public $region;
    public $descripcion;
    public $proyecto;
    public $contexto;
    public $area;
    public $email;
    public $telefono;
    public $usuario;
    public $pass;
    public $tipo;
    public $otroSolicitante;
    public $id;
    public $informacion;

    /**
     * @return mixed
     */
    public function getInformacion()
    {
        return $this->informacion;
    }

    /**
     * @param mixed $informacion
     */
    public function setInformacion($informacion)
    {
        $this->informacion = $informacion;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOtroSolicitante()
    {
        return $this->otroSolicitante;
    }

    /**
     * @param mixed $otroSolicitante
     */
    public function setOtroSolicitante($otroSolicitante)
    {
        $this->otroSolicitante = $otroSolicitante;
    }

    /**
     * @return mixed
     */
    public function getOtroContexto()
    {
        return $this->otroContexto;
    }

    /**
     * @param mixed $otroContexto
     */
    public function setOtroContexto($otroContexto)
    {
        $this->otroContexto = $otroContexto;
    }
    public $otroContexto;

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }



    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getSolicitante()
    {
        return $this->solicitante;
    }

    /**
     * @param mixed $solicitante
     */
    public function setSolicitante($solicitante)
    {
        $this->solicitante = $solicitante;
    }

    /**
     * @return mixed
     */
    public function getDependencia()
    {
        return $this->dependencia;
    }

    /**
     * @param mixed $dependencia
     */
    public function setDependencia($dependencia)
    {
        $this->dependencia = $dependencia;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getProyecto()
    {
        return $this->proyecto;
    }

    /**
     * @param mixed $proyecto
     */
    public function setProyecto($proyecto)
    {
        $this->proyecto = $proyecto;
    }

    /**
     * @return mixed
     */
    public function getContexto()
    {
        return $this->contexto;
    }

    /**
     * @param mixed $contexto
     */
    public function setContexto($contexto)
    {
        $this->contexto = $contexto;
    }

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param mixed $area
     */
    public function setArea($area)
    {
        $this->area = $area;
    }


    public function add(){
        $conexion = new db();
        $conn = $conexion->connection();

            $nombre= $this->getNombre();
            $email = $this -> getEmail();
            $solicitante = $this->getSolicitante();
            $dependencia =  $this->getDependencia();
            $region = $this->getRegion();
            $area = $this->getRegion();
            $descripcion = $this -> getDescripcion();
            $proyecto = $this->getProyecto();
            $contexto = $this->getContexto();
            $fecha = $this -> getFecha();
            $telefono = $this->getTelefono();
            $tipo = $this->getTipo();
            $otroSolicitante = $this->getOtroSolicitante();
            $otroContexto = $this->getOtroContexto();
            $folio = $this->folio();
            $informacion = $this->getInformacion();


            try{
                $sql = "INSERT INTO request_uv_formato (nombre,email,solicitante,dependencia,region,area,descripcion,
                        proyecto,contexto_proyecto,fecha,telefono,tipoServicio,otroSolicitante,otroContextoProyecto,folio,informacion) 
                        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $query = $conn->prepare($sql);
                $query->bindParam(1,$nombre);
                $query->bindParam(2,$email);
                $query->bindParam(3,$solicitante);
                $query->bindParam(4,$dependencia);
                $query->bindParam(5,$region);
                $query->bindParam(6,$area);
                $query->bindParam(7,$descripcion);
                $query->bindParam(8,$proyecto);
                $query->bindParam(9,$contexto);
                $query->bindParam(10,$fecha);
                $query->bindParam(11,$telefono);
                $query->bindParam(12,$tipo);
                $query->bindParam(13,$otroSolicitante);
                $query->bindParam(14,$otroContexto);
                $query->bindParam(15,$folio);
                $query->bindParam(16,$informacion);
                $result = $query->execute();
                $this->setId($conn->lastInsertId());


                return $result;
            }catch (PDOException $e){
                echo $e->getMessage();
                die();
            }
    }

    public function emailCuo (){
        $email =  $this->getEmail();
        $email =  $this->getEmail();
        $me ="solicitud enviada .'$email'.";

        $mensaje = 'solicitud de informacion (SI-02) solicitada por:'.$email;

        $detalleFinal = "
<li>Reporte solicitado por:  ".$this->getNombre()."</li>
<li>Tipo de solicitud:  ".$this->getTipo()."</li>
<li> crear reporte PDF: <a href='https://www.uv.mx/apps/cuo/Solicitudes-Cuo/pdf.php?id=".$this->getId()."'> enlace</a>  </li>
<li>Correo:  ".$this->getEmail()."</li>
<li>Ingresar al sistema: Aqui va el enlace: <a href='https://www.uv.mx/apps/cuo/Solicitudes-Cuo/login.php'>Enlace</a> </li>



";


        $emailFrom = 'univcuo@gmail.com';
        $contra = 'njjkqnrruesgegkt';
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->CharSet = 'utf-8';
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $mail->SMTPAuth = true;
        $mail->Username = $emailFrom;
        $mail->Password = $contra;
        $mail->setFrom('josecastillo03@uv.mx', 'solicitud de informacion (SI-02)1:');
        $mail->addReplyTo('irios@uv.mx', 'solicitud de informacion (SI-02)2');
        $mail->addAddress('irios@uv.mx');
        $mail->Subject = 'solicitud de informacion (SI-02)3';
        $mail->msgHTML($detalleFinal);
        //Replace the plain text body with one created manually
        $mail->AltBody = 'This is a plain-text message body';

        if (!$mail->send()) {
            return $mail->ErrorInfo;
        } else {
            return true;
            }
    }



    public function session(){

        $conexion = new  db();
        $conn = $conexion->connection();
        $password = $this->getPassword();


        try{
            $sql ="select * from user_uv_formato uuvf where cop.password = '$password' ";
            $query = $conn->query($sql);
            $result = $query->fetchColumn();
            return $result;

        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }
    }


    public function login(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $password = $this->getPass();
        $user = $this->getUsuario();

        //var_dump($password);
        //select * from user_uv_formato uuf where uuf.user = 'admin' and  uuf.password = 'admincuo_@';

        try{
            $sql ="select * from user_uv_formato uuf where uuf.user = '$user' and  uuf.password = '$password';";
            $query = $conn->query($sql);
            $result = $query->fetchColumn();
            return $result;
        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }
    }

    public function AsignarUser(){
        $_SESSION['Usuario'] = $this->getUsuario();
    }

    public function folio (){
        $conexion = new DB();
        $conn = $conexion->connection();

        try{
            $sql = "select max(id_request) from request_uv_formato ruf order by id_request desc";
            $query = $conn->query($sql);
            $result = $query->fetchColumn();

            $incremento = intval($result) +1;

            $fechaHoy  = date("Ymd");
            $folio= 'SI-02-'.$fechaHoy.'-'.$incremento;
            return $folio;

        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }



    }

}