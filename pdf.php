<?php

include_once  'Modal/fpdf182/fpdf.php';
include_once 'Config/db.php';


class PDF extends FPDF
{


// Cabecera de página
    function Header()
    {

        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Movernos a la derecha
        $this->Cell(80);
        // Título
        $this->Cell(30,10,'UNIVERSIDAD VERACRUZANA ',0,1,'C',0);
        $this->Ln(5);
        $this->Cell(80);
        $this->Cell(30,10,'COORDINACION UNIVERSITARIA DE OBSERVATORIOS',0,1,'C',0);
        // Salto de línea
        $this->Ln(5);
        $this->Cell(80);
        $this->Cell(30,10,utf8_decode('Formulario de solicitud de información (SI-02)'),0,1,'C',0);
        $this->Ln(20);

    }

// Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10, utf8_decode('Página').$this->PageNo().'/{nb}',0,0,'C');
    }

    function conten($id){

        $i= $id;

        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from request_uv_formato where id_request ='$i'";


        //var_dump($sql);
        $inf = $conn->prepare($sql);
        $inf->execute();

        //var_dump($inf);
        return $result = $inf->fetchAll(PDO::FETCH_ASSOC);
    }
}

// Creación del objeto de la clase heredada

$id = $_REQUEST['id'];




$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);
//$valor = $pdf->conten($id);


$data = $pdf->conten($id);
//var_dump($data[0]['email']);


$pdf->Cell(90,10,utf8_decode('Fecha de solicitud: ').$data[0]['fecha'],0,0,'c',0);
$pdf->Ln(8);

$pdf->Cell(90,10,utf8_decode('Tipo servicio: ').$data[0]['tipoServicio'], 0,0,'c',0);
$pdf->Ln(8);

$pdf->Cell(90,10,utf8_decode('Nombre: ').$data[0]['nombre'], 0,0,'c',0);
$pdf->Ln(8);
$pdf->Cell(90,10,utf8_decode('Email: ').$data[0]['email'], 0,0,'c',0);
$pdf->Ln(8);
$pdf->Cell(90,10,utf8_decode('Telefono: ').$data[0]['telefono'],0,0,'c',0 );
$pdf->Ln(8);
$pdf->Cell(90,10,utf8_decode('Solicitante: ').$data[0]['solicitante'],0,0,'c',0);
$pdf->Ln(8);
$pdf->Cell(90,10,utf8_decode('Nombre de la Entidad o Dependencia de la UV: ').$data[0]['dependencia'],0,0,'c',0);
$pdf->Ln(8);
$pdf->Cell(90,10,utf8_decode('Región universitaria: ').$data[0]['region'],0,0,'c',0);
$pdf->Ln(8);
$pdf->Cell(90,10,utf8_decode('Área académica: ').$data[0]['area'],0,0,'c',0);

$pdf->Ln(8);
$pdf->Cell(90,10,utf8_decode('Información solicitada: ').$data[0]['informacion'],0,0,'c',0);

$pdf->Ln(8);
$pdf->Cell(90,10,utf8_decode('Descripción de la información solicitada:').$data[0]['descripcion'],1,1,'c',0);

$pdf->Ln(8);
$pdf->Cell(90,10,utf8_decode('Nombre del proyecto: ').$data[0]['proyecto'],0,0,'c',0);

$pdf->Ln(8);
$pdf->Cell(90,10,utf8_decode('Contexto del proyecto: ').$data[0]['contexto_proyecto'],0,0,'c',0);

$pdf->Output();
?>

