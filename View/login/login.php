<section>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 m-2">
                <h5>Inicio de seccion</h5>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 cont-login  ">
                <div class="card">
                    <div class="card-body">
                        <form action="" id="formLogin">
                            <div class="form-group">
                                <label for="email">Usuario:</label>
                                <input type="email" class="form-control" placeholder="usuario" name="usuario" id="usuario">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" class="form-control" placeholder="Contraseña" name="pass" id="pass">
                            </div>
                        </form>
                        <button type="button" id="ingresar" class="btn btn-primary">Ingresar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
