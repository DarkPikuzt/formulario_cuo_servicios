<style>
    #otro_solicitante{
        display: none;
    }

    #otro_contexto{
        display: none;
    }
</style>





<div class="container" >
    <div class="row align-items-center">
        <div class="col-12 text-center m-2">
            <h3>UNIVERSIDAD VERACRUZANA</h3>
            <h4>COORDINACIÓN UNIVERSITARIA DE OBSERVATORIOS </h4>
            <h5>Formulario de solicitud de información o asesoria (SI-02)</h5>
        </div>


        <div class="col-12 align-items-center">

            <div class="card">
                <div class="card-body">
                    <form id="form">
                        <div class="row">

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="opError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Tipo:</label>
                                    <div class="col-sm-8 text-letf">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="op" id="op" value="Asesoria">
                                            <label class="form-check-label" for="inlineCheckbox1">Asesoria</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="op" id="op" value="Informacion">
                                            <label class="form-check-label" for="inlineCheckbox2">Información</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="fechaError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Fecha de solicitud:</label>
                                    <div class="col-sm-8">
                                        <input type="datetime-local" name="fecha" id="fecha" class="form-control-plaintext">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="nombreError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Nombre:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="nombre" id="nombre" class="form-control" id="inputPassword"
                                               placeholder="Nombre">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="emailError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Email (De preferencia correo UV):</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="email" id="email" class="form-control" id="inputPassword"
                                               placeholder="email">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="solicitanteError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Solicitante:</label>
                                    <div class="col-sm-8">
                                        <select  name="solicitante" id="solicitante" class="custom-select"  >
                                            <option>Seleccionar </option>
                                            <option>Docente</option>
                                            <option>Investigador/a </option>
                                            <option>Técnico académico </option>
                                            <option>Estudiante </option>
                                            <option>Administrativo  </option>
                                            <option>Observatorio  </option>
                                            <option>otro</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="otro_solicitante">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Mencione cual otro solicitante :</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="otroSol" id=" otroSol" class="form-control"
                                               placeholder="Otro solicitante">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="entidadError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Nombre de la Entidad o Dependencia de la UV:</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="dependencia" id="dependencia" class="form-control" id="inputPassword"
                                               placeholder="Nombre">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="telefonoError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Teléfono de contacto (opcional):</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="telefono" id="telefono" class="form-control" id="inputPassword"
                                               placeholder="telefono">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="regionError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Región universitaria:</label>
                                    <div class="col-sm-8">
                                        <select name="region" id="region" class="custom-select" >
                                            <option>Seleccionar </option>
                                            <option>Xalapa</option>
                                            <option>Veracruz </option>
                                            <option>Orizaba – Córdoba  </option>
                                            <option>Poza Rica -Tuxpan  </option>
                                            <option>Coatzacoalcos – Minatitlán  </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="areaError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Área académica:</label>
                                    <div class="col-sm-8">
                                        <select  class="custom-select" name="area" id="area">
                                            <option>Seleccionar </option>
                                            <option>Técnica</option>
                                            <option>Humanidades  </option>
                                            <option>Económico – Administrativa  </option>
                                            <option>Artes </option>
                                            <option>Ciencias de la Salud  </option>
                                            <option>Ciencias Biológicas y Agropecuarias </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="informacionError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Información solicitada:</label>
                                    <div class="col-sm-8">
                                        <select  class="custom-select" name="informacion" id="informacion">
                                            <option>Seleccionar </option>
                                            <option>Cartografía</option>
                                            <option>Datos   </option>
                                            <option>Indicadores   </option>
                                            <option>Imágenes de satélite </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="descripcionError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Descripción de la información o asesoría solicitada:</label>
                                    <div class="col-sm-8">
                                        <textarea  class="form-control" name="descripcion" id="descripcion" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>



                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="proyectoError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Nombre del proyecto:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="proyecto" id="proyecto"
                                               placeholder="Nombre del proyecto">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="contextError">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-4 text-left col-form-label">Contexto del proyecto:</label>
                                    <div class="col-sm-8">
                                        <select  class="custom-select" name="contexto" id="contexto">
                                            <option>Seleccionar</option>
                                            <option>Económico </option>
                                            <option>Social   </option>
                                            <option>Salud   </option>
                                            <option>Educación  </option>
                                            <option>Demografía  </option>
                                            <option>Medio Ambiente  </option>
                                            <option>Seguridad   </option>
                                            <option>Otro </option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="otro_contexto">
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 text-left col-form-label">Describa el otro contexto del proyecto :</label>
                                <div class="col-sm-8">
                                    <input type="text" name="otroContx" id=" otroContx" class="form-control"
                                           placeholder="Otro Contexto">
                                </div>
                            </div>
                        </div>



                        <style>
                            .mes-error{
                                position: absolute;
                                top: 5px;
                                right: 0;
                            }
                        </style>

                    </form>
                </div>
            </div>
        </div>
        <div class="col-12  ">
            <div class="card ">
                <div class="card-body">
                    <p><strong>Condiciones de entrega de la información:</strong></p>
                    <p>La Coordinación Universitaria de Observatorios proporcionará la información al solicitante bajo los siguientes criterios:
                    </p>
                    <ol type="a">
                        <li>El solicitante no deberá comercializar, conceder, ni distribuir copias de la información proporcionada.</li>
                        <li>El solicitante deberá otorgar de manera pública los créditos correspondientes de la fuente generadora de información.</li>
                        <li>La CUO no se hará responsable por la interpretación y aplicación que el solicitante haga de los resultados obtenidos a través del uso de la información entregada.
                        </li>
                        <li>La CUO hace una atenta invitación al solicitante de compartir los resultados de sus proyectos o investigaciones, otorgando los créditos correspondientes.
                        </li>
                        <li>Este servicio es proporcionado a toda la comunidad universitaria sin costo alguno.</li>
                    </ol>

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 ">
                        <button type="button" class="btn btn-primary" id="enviar">Enviar</button>
                        <div class="mes-error" id="error">
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>
</div>

<div class="modal" id="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mensaje</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Una vez recibida la solicitud, personal de la Coordinación Universitaria de Observatorios establecerá comunicación con el solicitante para dar atención y seguimiento.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>
