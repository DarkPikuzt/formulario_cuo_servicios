
<?php

require_once 'Config/db.php';

$db = new db();

$conn = $db ->connection();

$sql = "select * from request_uv_formato ruf;";
$query = $conn->prepare($sql);
$result = $query->execute();
$data = $query->fetchAll(PDO::FETCH_ASSOC);
?>
<h1 class="text-center">Informe</h1>
<h3 class="text-center">solicitudes de información (SI-02)</h3>
<div class="container">
    <div class="row">
        <div class="col-12 table-responsive">
            <table id="example12" class="table table-striped table-bordered table-hover " style="width:100%">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Identidad</th>
                    <th>Email</th>
                    <th>PDF</th>
                </tr>
                </thead>
                <tbody>

                <?php
               foreach ($data as $item):
                ?>
                <tr>
                    <td><?php echo $item['nombre'] ?></td>
                    <td><?php echo $item['dependencia'] ?></td>
                    <td><?php echo $item['email'] ?></td>
                    <td class="text-center"> <a href="pdf.php?id=<?php echo $item['id_request']?>" target="_blank"><i class="fas fa-file-pdf" style="color: brown; size: px;"></i></a> </td>
                </tr>
               <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script>

    $(document).ready(function () {


        $('#example12').DataTable({
            "language": {
                "emptyTable": "No hay datos disponibles en la tabla.",
                "info": "Del _START_ al _END_ de _TOTAL_ ",
                "infoEmpty": "Mostrando 0 registros de un total de 0.",
                "infoFiltered": "(filtrados de un total de _MAX_ registros)",
                "infoPostFix": "(actualizados)",
                "lengthMenu": "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "searchPlaceholder": "Dato para buscar",
                "zeroRecords": "No se han encontrado coincidencias.",
                "paginate": {
                    "first": "Primera",
                    "last": "Última",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "aria": {
                    "sortAscending": "Ordenación ascendente",
                    "sortDescending": "Ordenación descendente"
                }
            }

        });
    });

</script>
